# Welcome to Ninkendi, scholar


## Introduction

This a very short game about a new ASM programmer that starts working for the very famous Ninkendi videogames company, but soon discovers something's weird about the assignments. Exploring different notebooks with the assigment details, the programmer find binary, hex and history clues. Is this programmer asked to participate in a regular game project or is it something more?

## Characters

The scholar: A new ASM programmer in Ninkendi.
The boss: Your boss doesn't say much to you. She's only over there and doesn't let you touch the computer.


## Motivation

I got involved in a [game jam](https://itch.io/jam/mini-jam-64) for the first time in several years because I love classic consoles and this one got my attention. Although I work in tech I'm not used to create videogames and thought about this as a challenge.


## Tools

I use [bitsy]() for the main program and S.A.M.M.I app for the music, and heroku for the deployment. 

## Repo

The game is open source, you can check the bundle in the [game_bundle]() directory, and I will also attach some art I made only for my inspiration, even though the game graphics are way more simple. 


In the game I attached a Easter Egg about a lateral project of console designing, although that one is entirely in japanese. 

